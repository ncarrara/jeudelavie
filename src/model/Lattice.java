/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import model.Noeud.Etat;
import static model.Noeud.Etat.MORT;
import static model.Noeud.Etat.VIVANT;

/**
 *
 * @author CARRARA Nicolas <nicolas.carrara1@etu.univ-lorraine.fr>
 */
public class Lattice implements Comparable{

    @Override
    public int compareTo(Object o) {
        return ((Lattice)o).equals(this)?0:-1;
    }
    
    @Override
    public boolean equals(Object o) {
        return Arrays.deepEquals(this.configuration, ((Lattice) o).configuration);
    }

    @Override
    public Object clone() {
        Lattice l = new Lattice(size);
        l.configuration = new Noeud[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                l.configuration[i][j] = (Noeud) configuration[i][j].clone();
            }

        }
//        System.out.println(""+l);
        return l;
    }

    private Noeud[][] configuration;

    

    private int size;

    private int currentAliveCells = 0;

    public Lattice(int size) {
        this.size = size;
    }

    public Lattice(int size, double random) {
        this.size = size;
        configuration = new Noeud[size][size];
        construct(random);
        constructVoisin();
    }

    public Lattice(int[][] matrice) {
        this.size = matrice[0].length;
        configuration = new Noeud[size][size];
        construct(matrice);
        constructVoisin();
    }

    private void construct(double random) {
        for (int i = 0; i < getSize(); i++) {
            for (int j = 0; j < getSize(); j++) {
                Etat e = Etat.getRandomState(random);
                Noeud n = new Noeud(e);
                getConfiguration()[i][j] = n;
            }
        }
    }

    private void construct(int[][] matrice) {
//        System.out.println(""+Utils.toStringTab(matrice));
        for (int i = 0; i < getSize(); i++) {
            for (int j = 0; j < getSize(); j++) {
                Noeud n = new Noeud(matrice[i][j] == 1 ? VIVANT : MORT);
                getConfiguration()[i][j] = n;
            }
        }

    }

    private void constructVoisin() {
        for (int i = 0; i < getSize(); i++) {
            for (int j = 0; j < getSize(); j++) {
                int io = (i - 1) < 0 ? getSize() - 1 : i - 1;
                int ie = (i + 1) % getSize();
                int jn = (j - 1) < 0 ? getSize() - 1 : (j - 1);
                int js = (j + 1) % getSize();

                Noeud nord = getConfiguration()[i][jn];
                Noeud sud = getConfiguration()[i][js];
                Noeud est = getConfiguration()[ie][j];
                Noeud ouest = getConfiguration()[io][j];

                Noeud nordouest = getConfiguration()[io][jn];
                Noeud sudest = getConfiguration()[ie][js];
                Noeud nordest = getConfiguration()[ie][jn];
                Noeud sudouest = getConfiguration()[io][js];

                Noeud here = getConfiguration()[i][j];
                List<Noeud> voisins = new ArrayList<>(8);
                voisins.add(nord);
                voisins.add(est);
                voisins.add(ouest);
                voisins.add(sud);
                voisins.add(nordest);
                voisins.add(nordouest);
                voisins.add(sudest);
                voisins.add(sudouest);
                here.setVoisins(voisins);
            }
        }
//        System.out.println("configuration \n"+this);
    }

    public void compute() {
        currentAliveCells = 0;
        for (int i = 0; i < getSize(); i++) {
            for (int j = 0; j < getSize(); j++) {
                getConfiguration()[i][j].compute();
            }
        }
        postCompute();
    }

    private void postCompute() {
        for (int i = 0; i < getSize(); i++) {
            for (int j = 0; j < getSize(); j++) {
                configuration[i][j].postCompute();
                if (configuration[i][j].getEtatCourant() == VIVANT) {
                    currentAliveCells++;
                }
            }
        }
    }

    @Override
    public String toString() {
//        return Arrays.deepToString(getLattice());
        String s = "";
        for (int i = 0; i < getSize(); i++) {
            for (int j = 0; j < getSize(); j++) {
                s += configuration[i][j] + "\t\t";
            }
            s += "\n";
        }
        return s;
    }

    /**
     * @return the configuration
     */
    public Noeud[][] getConfiguration() {
        return configuration;
    }

    /**
     * @param configuration the configuration to set
     */
    public void setConfiguration(Noeud[][] configuration) {
        this.configuration = configuration;
    }

    /**
     * @return the size
     */
    public int getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * @return the currentAliveCells
     */
    public int getCurrentAliveCells() {
        return currentAliveCells;
    }

    /**
     * @param currentAliveCells the currentAliveCells to set
     */
    public void setCurrentAliveCells(int currentAliveCells) {
        this.currentAliveCells = currentAliveCells;
    }
}
