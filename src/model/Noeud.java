/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import static model.Noeud.Etat.MORT;
import static model.Noeud.Etat.VIVANT;

/**
 *
 * @author CARRARA Nicolas <nicolas.carrara1@etu.univ-lorraine.fr>
 */
public class Noeud {

    public enum Etat {

        VIVANT, MORT;

        public static Etat getRandomState(double random) {
            double d = Math.random();
            if (d > random) {
                return VIVANT;
            } else {
                return MORT;
            }
        }
    }
    
    @Override
    public boolean equals(Object o){
        return ((Noeud)o).etatCourant == this.etatCourant;
    }
    
    @Override
    public Object clone(){
        return new Noeud(etatCourant);
    }

    private Etat etatFutur;

    private Etat etatCourant;

    private List<Noeud> voisins;

    public Noeud(Etat e) {
        etatCourant = e;
    }

    public void compute() {
        int vivants = getVoisinsVivants();
        switch (etatCourant) {
            case MORT:
                etatFutur = (vivants == 3) ? VIVANT : MORT;
                break;
            case VIVANT:
                etatFutur = (vivants == 2 || vivants == 3) ? VIVANT : MORT;
                break;
        }
    }

    public void postCompute() {
        etatCourant = etatFutur;
    }

    private int getVoisinsVivants() {
        // on regarde l'etat precedent des voisins
        int vivants = 0;
        for (Noeud v : getVoisins()) {
            if (v.getEtatCourant() == VIVANT) {
                vivants++;
            }
        }
        return vivants;
    }

    /**
     * @return the etatFutur
     */
    public Etat getEtatFutur() {
        return etatFutur;
    }

    /**
     * @return the etatCourant
     */
    public Etat getEtatCourant() {
        return etatCourant;
    }

    public void setEtatFutur(Etat etatCourant) {
        this.etatFutur = etatCourant;
    }

    public void setEtatCourant(Etat etatCourant) {
        this.etatCourant = etatCourant;
    }

    public String toString() {
        return etatCourant + "";

    }

    /**
     * @return the voisins
     */
    public List<Noeud> getVoisins() {
        return voisins;
    }

    /**
     * @param voisins the voisins to set
     */
    public void setVoisins(List<Noeud> voisins) {
        this.voisins = voisins;
    }

}
