/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CARRARA Nicolas <nicolas.carrara1@etu.univ-lorraine.fr>
 */
public class Model extends Observable {

    private final static String SIZE = "size";
    private final static String ITERATIONS_MAX = "iterationsMax";
    private final static String RANDOM = "random";
    private final static String WRITE_STATS = "writeStats";

    private final static String CSV = "csv";

    private Lattice lattice;

    private int size;

    private String csv;
    private int iterationsMax;

    private double random;

    private Properties properties;

    private List<Lattice> lattices;

    private int runs = 0;

    private final int UNDEFINED = -1;

    private int periode = UNDEFINED;

    private boolean writeStats = false;

    public Model(Properties properties) throws IOException {
        lattices = new ArrayList<>();
        this.properties = properties;
        //iterations
        String s = properties.getProperty(Model.ITERATIONS_MAX);
        iterationsMax = s == null ? Integer.MAX_VALUE : Integer.parseInt(s);
        String stats = properties.getProperty(Model.WRITE_STATS);
        writeStats = stats == null ? false : Boolean.parseBoolean(stats);
        // csv ou random init ?
        csv = properties.getProperty(CSV);
        if (csv != null) {
            int[][] mat = Utils.parseCSVFile(new File(getCsv()));
            lattice = new Lattice(mat);
            size = mat.length;
        } else {
            size = Integer.parseInt(properties.getProperty(SIZE));
            random = Double.parseDouble(properties.getProperty(Model.RANDOM));
            lattice = new Lattice(getSize(), getRandom());
        }
    }

    public Model(int iterationsMax, int size, double random, boolean writeStats) {
        this.writeStats = writeStats;
        lattices = new ArrayList<>();
        this.iterationsMax = iterationsMax;
        this.size = size;
        this.random = random;
        lattice = new Lattice(size, random);
    }

    public void run(int computation) {
        int i = 0;
        while (i < computation) {
            runAtomique();
            i++;
        }

    }

    public void reset() {
//        size = Integer.parseInt(properties.getProperty(SIZE));
//        iterationsMax = Integer.parseInt(properties.getProperty(Model.ITERATIONS));
//        lattice = new Lattice(getSize(), random);
//        csv = properties.getProperty(CSV);
        if (getCsv() != null) {
            setLattice(new Lattice(Utils.parseCSVFile(new File(getCsv()))));
        } else {
//            random = Double.parseDouble(properties.getProperty(Model.RANDOM));
            setLattice(new Lattice(getSize(), getRandom()));
        }
        setRuns(0);
        periode = UNDEFINED;
        lattices.clear();
        update();
    }

    public void runAtomique() {
        setRuns(getRuns() + 1);
        getLattice().compute();
        Lattice newLattice = (Lattice) getLattice().clone();
        // un peu sale
        int index;
        if (periode == UNDEFINED) {
            if ((index = getLattices().lastIndexOf(newLattice)) != -1) {
                periode = getRuns() - index - 2;
                if (writeStats) {
                    writeStats();
                }
            } else {
                getLattices().add(newLattice);
                if (getRuns() == getIterationsMax()) {
                    if (writeStats) {
                        writeStats();
                    }
                }
            }
        }
        update();
    }

    private void writeStats() {
        System.out.println("writing statistics ...");
        System.out.println("periode : " + periode);
        System.out.println("runs pour cette periode : " + (getRuns() == getIterationsMax() ? "max" : getRuns()));

        FileWriter fw = null;
        File f = new File("statistiques" + size + ".csv");
        if (f.exists()) {

        } else {
            try {
                f.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            fw = new FileWriter(f, true);
            fw.append((getRuns() == getIterationsMax() ? "max" : getRuns()) + " " + periode);
            fw.append("\n");
        } catch (IOException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

//    public void writeIterationsMax(){
//        FileWriter fw = null;
//                    try {
//
//                        
//                        fw = new FileWriter(new File("statistiques" + size + ".csv"), true);
//                        fw.append("maxIteration" + getIterationsMax() + " -1");
//                        fw.append("\n");
//                    } catch (IOException ex) {
//                        Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
//                    } finally {
//                        try {
//                            fw.close();
//                        } catch (IOException ex) {
//                            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
//                        }
//                    }
//    }
    public void update() {
        setChanged();
        notifyObservers();
    }

    public void run() {
        run(getIterationsMax());
    }

    public void setSize(int size) {
        this.size = size;
    }

    /**
     * @return the lattice
     */
    public Lattice getLattice() {
        return lattice;
    }

    /**
     * @param lattice the lattice to set
     */
    public void setLattice(Lattice lattice) {
        this.lattice = lattice;
    }

    /**
     * @return the size
     */
    public int getSize() {
        return size;
    }

    /**
     * @return the properties
     */
    public Properties getProperties() {
        return properties;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    /**
     * @return the csv
     */
    public String getCsv() {
        return csv;
    }

    /**
     * @param csv the csv to set
     */
    public void setCsv(String csv) {
        this.csv = csv;
    }

    /**
     * @return the iterationsMax
     */
    public int getIterationsMax() {
        return iterationsMax;
    }

    /**
     * @param iterationsMax the iterationsMax to set
     */
    public void setIterationsMax(int iterationsMax) {
        this.iterationsMax = iterationsMax;
    }

    /**
     * @return the random
     */
    public double getRandom() {
        return random;
    }

    /**
     * @param random the random to set
     */
    public void setRandom(double random) {
        this.random = random;
    }

    /**
     * @return the lattices
     */
    public List<Lattice> getLattices() {
        return lattices;
    }

    /**
     * @param lattices the lattices to set
     */
    public void setLattices(List<Lattice> lattices) {
        this.lattices = lattices;
    }

    /**
     * @return the runs
     */
    public int getRuns() {
        return runs;
    }

    /**
     * @param runs the runs to set
     */
    public void setRuns(int runs) {
        this.runs = runs;
    }

}
