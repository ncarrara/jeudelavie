package main;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import model.Model;
import view.Frame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author CARRARA Nicolas <nicolas.carrara1@etu.univ-lorraine.fr>
 */
public class Main {

    public static void main(String[] args) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream("parameters.properties"));
        boolean showGui = Boolean.parseBoolean(properties.getProperty("showgui"));
        Model model = new Model(properties);
        if (showGui) {
            new Frame(model);
            //model.run();
        } else {
            System.out.println("avant \n\n"+model.getLattice());
            model.run();
            System.out.println("après \n\n"+model.getLattice());
        }
        
    }

}
