/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import model.Model;

/**
 *
 * @author CARRARA Nicolas <nicolas.carrara1@etu.univ-lorraine.fr>
 */
public class MainStatistiques {

    public static void main(String[] args) {

        int[] taille = {5, 10, 20, 50, 100};
        int iterationMax = 1000;
        double random = 0.7;
        int k = 0;
        while (k < 1000) {
            for (int i = 0; i < taille.length; i++) {
                Model m = new Model(iterationMax, taille[i], random, true);
                m.run();
            }
            k++;
        }
    }
}
