/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import model.Model;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author CARRARA Nicolas <nicolas.carrara1@etu.univ-lorraine.fr>
 */
public class PeriodeTest {

    public PeriodeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void hello() throws FileNotFoundException, IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream("periodes.properties"));
        Model m = new Model(properties);
//        System.out.println("" + m.getLattice());
        System.out.println("" + m.getLattices());
        System.out.println("-----------------------------------------------");
        m.runAtomique();
        System.out.println("" + m.getLattices());
        System.out.println("-----------------------------------------------");
//        System.out.println("" + m.getLattice());
        m.runAtomique();
        System.out.println("" + m.getLattices());
        System.out.println("-----------------------------------------------");
//        System.out.println("" + m.getLattice());
        m.runAtomique();
        System.out.println("" + m.getLattices());
        System.out.println("-----------------------------------------------");
//        System.out.println("" + m.getLattice());
        m.runAtomique();
//        System.out.println("" + m.getLattice());
        System.out.println("" + m.getLattices());
        System.out.println("-----------------------------------------------");

    }
}
